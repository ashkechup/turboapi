import os
from unittest import TestCase

from calculator.api.services.input_file_service import (
    XlsFileService,
    ColumnCount,
    ColumnNotInFileException,
)


class TestXlsFileService(TestCase):
    def setUp(self):
        test_data_dir = os.path.join(os.getcwd(), "data")
        input_file = os.path.join(test_data_dir, "example_test_file.xlsx")

        self.service = XlsFileService(input_file)

    def test_read_sheets(self):
        sheets_in_file = 2
        data_frames = self.service._read_file()

        self.assertEqual(len(data_frames), sheets_in_file)

    def test_processing_data_frames(self):
        column_count = ColumnCount(name="CAD", sum_=7818.35, average=60.61, items=129)
        data_frames = self.service._read_file()
        cols_count = self.service._process_data_frames(
            cols=["CAD"], data_frames=data_frames
        )
        self.assertEqual([column_count], cols_count)

    def test_column_not_exists(self):
        data_frames = self.service._read_file()
        self.assertRaises(
            ColumnNotInFileException,
            self.service._process_data_frames,
            cols=["Bulbasaur"],
            data_frames=data_frames,
        )
