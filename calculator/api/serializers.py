from rest_framework import serializers
from rest_framework.fields import FileField


class UploadSerializer(serializers.Serializer):
    file_uploaded = FileField()
    column_names = serializers.ListField()
