import math
import os
from dataclasses import dataclass
from typing import Union

import pandas as pd

from abc import abstractmethod


@dataclass()
class ColumnCount:
    """Storage of calculations for column"""

    name: str
    sum_: float = 0
    average: float = 0
    items: int = 0


@dataclass()
class DataIndex:
    """Concrete data record location"""

    row_index: int
    col_index: int


class ColumnNotInFileException(Exception):
    def __init__(self, column_name: str):
        super().__init__()
        self.column_name = column_name

    def __str__(self):
        return f"Column: {self.column_name} probably not exists in file"


class FileWrongExtension(Exception):
    def __init__(self, supported_extensions: list[str]):
        super().__init__()
        self.supported_extensions = supported_extensions

    def __str__(self):
        return f"Specified file does not support any of extensions: {self.supported_extensions}"


class InputFileService:
    """Base for specific file extension service"""

    def __init__(self, file):
        self.file = file

    @abstractmethod
    def calculate(
        self, cols: list[str]
    ) -> dict[str, Union[str, list[dict[str, Union[str, float]]]]]:
        """Count sum and average of file concrete columns
        :param cols: names of columns
        :return: dictionary with counted sum and average for specified columns
        """

    @abstractmethod
    def _read_file(self) -> dict[str, pd.DataFrame]:
        """Parse input file to pandas library DataFrame objects
        :return: dictionary with pandas DataFrame objects
        """

    @abstractmethod
    def _validate_file_extension(self):
        """Checks if file extension is supported by parser. Raises error elsewhere"""


class XlsFileService(InputFileService):
    """Excel files extension service"""

    SUPPORTED_EXTENSIONS = [".xls", ".xlsx", ".xlsm", ".xlsb", ".odf", ".odt"]

    def calculate(
        self, cols: list[str]
    ) -> dict[str, Union[str, list[dict[str, Union[str, float]]]]]:
        """Count sum and average of Excel file concrete columns
        :param cols: names of columns
        :return: dictionary with counted sum and average for specified columns
        """
        self._validate_file_extension()
        data_frames = self._read_file()
        cols_count = self._process_data_frames(cols, data_frames)

        resp = {
            "file": os.path.basename(self.file.name),
            "summary": [
                {
                    "column": col_count.name,
                    "sum": col_count.sum_,
                    "avg": col_count.average,
                }
                for col_count in cols_count
            ],
        }

        return resp

    def _read_file(self) -> dict[str, pd.DataFrame]:
        """Parse input Excel file to pandas library DataFrame objects
        :return: pandas DataFrame objects
        """
        excel_file = pd.ExcelFile(self.file)
        return pd.read_excel(excel_file, sheet_name=None, header=None)

    def _validate_file_extension(self):
        """Checks if file extension is supported by parser. Raises error elsewhere"""
        is_extension_supported = any(
            extension
            for extension in self.SUPPORTED_EXTENSIONS
            if self.file.name.endswith(extension)
        )

        if not is_extension_supported:
            raise FileWrongExtension(self.SUPPORTED_EXTENSIONS)

    def _process_data_frames(
        self, cols: list[str], data_frames: dict[str, pd.DataFrame]
    ) -> list[ColumnCount]:
        """
        Performs operations on pandas DataFrame objects to count necessary result.
        :param cols: names of columns
        :param data_frames: pandas DataFrame objects
        :return: calculation results for columns
        """
        columns_count = []
        column_exists = {}

        for col in cols:
            col_count = ColumnCount(name=col)
            columns_count.append(col_count)
            for df in data_frames.values():
                indexes = self._find_indexes(col, df)
                column_exists = self._validate_column(df, column_exists, col, indexes)

                indexes = self._filter_indexes(indexes, df, col)
                for index in indexes:
                    data = self._get_data_below_index(df, index)
                    sum_, items = self._count_sum_and_items_number(data)
                    col_count.sum_ += sum_
                    col_count.items += items

                    col_count.sum_ = round(col_count.sum_, 2)
            col_count.average = (
                round(col_count.sum_ / col_count.items, 2) if col_count.items > 0 else 0
            )
            self._validate_data(column_exists)

        return columns_count

    def _validate_column(
        self,
        df: pd.DataFrame,
        column_exists: dict[str, bool],
        col_name: str,
        indexes: list[DataIndex],
    ) -> dict[str, bool]:
        """
        Saves info if currently processed column exists in processed DataFrame object
        :param df: pandas DataFrame object
        :param column_exists: storage of information if column exists in processed file
        :param col_name: name processed column
        :param indexes: locations of column in pandas DataFrame object
        :return: storage with updated information if column exists in processed file
        """
        col_exist = self._check_column_in_data_frame(df, col_name, indexes)
        if not column_exists.get(col_name, False):
            column_exists[col_name] = col_exist
        return column_exists

    @staticmethod
    def _find_indexes(column_name: str, df: pd.DataFrame) -> list[DataIndex]:
        """
        Finds possible positions in DataFrame object for concrete column name
        :param column_name: name of column to look for
        :param df: pandas DataFrame object
        :return: possible positions of column
        """
        return [
            DataIndex(
                row_index=df[col][
                    df[col].astype(str).str.contains(column_name, na=False)
                ].index[i],
                col_index=df.columns.get_loc(col),
            )
            for col in df.columns
            for i in range(
                len(
                    df[col][
                        df[col].astype(str).str.contains(column_name, na=False)
                    ].index
                )
            )
        ]

    def _check_column_name(
        self, df: pd.DataFrame, column_name: str, index: DataIndex
    ) -> bool:
        """
        Checks if record at specific position equals to currently processed column name
        :param df: pandas DataFrame object
        :param column_name: name of column to check
        :param index: location of column in pandas DataFrame object
        :return: bool
        """
        col_value = self._get_data_frame_value(df, index)
        if col_value.lower().strip() != column_name.lower().strip():
            return False
        return True

    @staticmethod
    def _get_data_frame_value(df: pd.DataFrame, index: DataIndex) -> str:
        """
        Get value at DataFrame concrete position
        :param df: pandas DataFrame object
        :param index: location of column in pandas DataFrame object
        :return: value at DataFrame position
        """
        return str(df.iloc[index.row_index, index.col_index])

    def _filter_indexes(
        self, indexes: list[DataIndex], df: pd.DataFrame, column_name: str
    ):
        """
        Filters column indexes by comparing values of DataFrame object at concrete position
        :param indexes: locations of column in pandas DataFrame object
        :param df: pandas DataFrame object
        :param column_name: name of column to check
        """
        return [
            index
            for index in indexes
            if self._check_column_name(df, column_name, index)
        ]

    def _check_column_in_data_frame(
        self, df: pd.DataFrame, column_name: str, indexes: list[DataIndex]
    ) -> bool:
        """
        Checks if specified column exists in any of provided index
        :param df: pandas DataFrame object
        :param column_name: name of column to check
        :param indexes: locations of column in pandas DataFrame object
        :return: bool
        """
        return any(self._check_column_name(df, column_name, index) for index in indexes)

    def _get_data_below_index(
        self, df: pd.DataFrame, index: DataIndex
    ) -> list[Union[float, int]]:
        """
        Gets list of values in concrete DataFrame object column
        :param df: pandas DataFrame object
        :param index: location of column in pandas DataFrame object
        :return: data in a specific column
        """
        data = df.iloc[index.row_index :, index.col_index]

        return self._filter_non_numeric_data(list(data))

    @staticmethod
    def _filter_non_numeric_data(data: list) -> list[Union[float, int]]:
        """
        Filters list with values in a specific column removing non numeric values
        :param data: data in a specific column
        :return: values which can be summed up
        """
        return [
            value
            for value in data
            if type(value) in [float, int] and not math.isnan(value)
        ]

    @staticmethod
    def _count_sum_and_items_number(
        data: list[Union[float, int]]
    ) -> tuple[Union[int, float], Union[int, float]]:
        """
        Counts sum of values and and number of values in provided container
        :param data: collection of values
        :return: sum and number of values for provided container
        """
        if not data:
            return 0, 0
        sum_ = sum(data)
        items = len(data)

        return sum_, items

    @staticmethod
    def _validate_data(column_exists: dict[str, bool]):
        """
        Checks if column is in any sheet of file. Raises exception if column name can not be found in file.
        :param column_exists: storage of information if column exists in processed file
        """
        for col_name, col_exist in column_exists.items():
            if not col_exist:
                raise ColumnNotInFileException(column_name=col_name)
