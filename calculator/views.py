from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.viewsets import ViewSet

from calculator.api.serializers import UploadSerializer
from calculator.api.services.input_file_service import (
    XlsFileService,
    ColumnNotInFileException,
    FileWrongExtension,
)


class CalculateViewSet(ViewSet):
    parser_classes = (MultiPartParser,)
    serializer_class = UploadSerializer

    column_names_param = openapi.Parameter(
        name="column_names",
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_ARRAY,
        required=True,
        items=openapi.Items(type=openapi.TYPE_STRING),
    )

    file_param = openapi.Parameter(
        name="file",
        in_=openapi.IN_FORM,
        type=openapi.TYPE_FILE,
        required=True,
        description="Document",
    )
    swagger_params = [
        column_names_param,
        file_param,
    ]

    @swagger_auto_schema(
        operation_description="Upload excel file and column names list. "
        "If the columns and data are valid "
        "average and sum values will be calculated for specified column names.",
        operation_id="Calculate",
        tags=["calculate"],
        manual_parameters=swagger_params,
        responses={400: "Invalid data in uploaded file", 200: "Success"},
    )
    @action(detail=False, methods=["post"], name="Calculate")
    def calculate(self, request):
        file_uploaded = request.FILES.get("file")
        column_names = request.GET.get("column_names").split(",")

        service = XlsFileService(file=file_uploaded)
        try:
            result = service.calculate(column_names)
        except (ColumnNotInFileException, FileWrongExtension) as exc:
            return Response(status=HTTP_400_BAD_REQUEST, data={"detail": str(exc)})

        return Response(result, status=HTTP_200_OK, content_type="json")
