# Table of Contents

* [Installation](#installation)
* [Run project](#run project)
* [Endpoints](#endpoints)


# Installation
Install Python 3.10: 
https://www.python.org/downloads/release/python-3100/

Install project libraries:
```python -m pip install -r requirements.txt```


# Run project
Go to main project directory where `manage.py` file exists and run:
`python manage.py runserver <port_number>`


# Endpoints
## Swagger
### `GET /swagger/`

## Docs
### `GET /redoc/`